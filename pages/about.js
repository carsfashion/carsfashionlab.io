import Link from 'next/link'
import Head from '../components/head'
import Nav from '../components/nav'

export default () => (
  <div>
    <Nav />

    <div className="hero">{ "About" }</div>
  </div>
)
